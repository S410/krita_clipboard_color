# Clipboard Color Grabber Plugin for Krita

### A _very_ simple plugin that yanks hex colors from the clipboard

![demo.webp](demo.webp)

### Installation:

-   Copy `clipboard_color` and `clipboard_color.desktop` into `pykrita` directory

    > On Linux with a system install of the program, the usual directory is  
    > `~/.local/share/krita/pykrita/`  
    > In case of Flatpak:  
    > `~/.var/app/org.kde.krita/data/krita/pykrita/`  
    > On Windows:  
    > `C:\Users\<user>\AppData\Roaming\krita\pykrita`  
    > On MacOS:  
    > `~/Library/Application Support/krita/pykrita`

-   Navigate `Settings` > `Configure Krita` > `Python Plugin Manager`
    and enable `Clipboard Color Grabber`

-   Restart Krita
