from krita import *
from PyQt5.Qt import QApplication
from typing import Optional


class ClipboardGrabber(Extension):
    def __init__(self, parent):
        super().__init__(parent)
        QApplication.clipboard().dataChanged.connect(self.onClipboardChanged)

    def setup(self):
        pass

    def createActions(self, window):
        pass

    def onClipboardChanged(self):
        self.grabColor()

    def grabColor(self):
        text = QApplication.clipboard().text().removeprefix("#")
        if not text:
            return

        components = self.parseColorHex(text)
        if not components:
            return

        color = ManagedColor("RGBA", "U8", "")
        color.setComponents(components)

        view = Krita.instance().activeWindow().activeView()
        view.setForeGroundColor(color)

    def parseColorHex(self, color: str) -> Optional[list[float]]:
        if color[0] == "#" and len(color) == 7:
            color = color[1:]
        elif len(color) != 6:
            return None

        try:
            color_int = int(color, 16)
        except:
            return None

        components = [0.0, 0.0, 0.0, 1.0]
        for x in range(3):
            color_int, c = divmod(color_int, 256)
            components[x] = c / 255

        return components


Krita.instance().addExtension(ClipboardGrabber(Krita.instance()))
